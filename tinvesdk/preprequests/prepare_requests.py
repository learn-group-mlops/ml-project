from datetime import datetime
from typing import Any, Callable, Literal, Optional, Type, TypeVar

from pydantic import parse_raw_as
from returns.future import FutureResult
from returns.result import Failure, Result, Success
from ..schemas.basemodel import BaseModel

__all__ = [
    "prepare_request",
]

S = TypeVar("S", bound=BaseModel)
F = TypeVar("F", bound=BaseModel)
Response = Result[S, F] | FutureResult[S, F]
Request = Callable[..., Response[S, F]]


def prepare_request(
    method: Literal["GET", "POST"],
    url: str,
    success: Type[S],
    failure: Type[F],
    params: Optional[dict[str, str | int | datetime]] = None,
) -> Request[S, F]:
    def prepared_request(request: Callable[..., Any]) -> Response[S, F]:
        response = request(method=method, url=url, params=params)
        match response.status_code:
            case 200:
                return Success(parse_raw_as(success, response.text))
            case 500:
                return Failure(parse_raw_as(failure, response.text))
            case _:
                return Failure(response.status_code)

    return prepared_request
