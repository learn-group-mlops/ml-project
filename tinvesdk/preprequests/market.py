from datetime import datetime, timezone
from typing import Literal

from ..schemas.error import Error
from ..schemas.market import (
    CandlesResponse,
    MarketInstrumentListResponse,
    OrderbookResponse,
    SearchMarketInstrumentResponse,
)

from .prepare_requests import Request, prepare_request

__all__ = [
    "get_market_stocks",
    "get_market_bonds",
    "get_market_etfs",
    "get_market_currencies",
    "get_market_orderbook",
    "get_market_candles",
    "get_market_search_by_figi",
    "get_market_search_by_ticker",
]


def get_market_stocks() -> Request[MarketInstrumentListResponse, Error]:
    return prepare_request(
        method="GET",
        url="/market/stocks",
        success=MarketInstrumentListResponse,
        failure=Error,
    )


def get_market_bonds() -> Request[MarketInstrumentListResponse, Error]:
    return prepare_request(
        method="GET",
        url="/market/bonds",
        success=MarketInstrumentListResponse,
        failure=Error,
    )


def get_market_etfs() -> Request[MarketInstrumentListResponse, Error]:
    return prepare_request(
        method="GET",
        url="/market/etfs",
        success=MarketInstrumentListResponse,
        failure=Error,
    )


def get_market_currencies() -> Request[MarketInstrumentListResponse, Error]:
    return prepare_request(
        method="GET",
        url="/market/currencies",
        success=MarketInstrumentListResponse,
        failure=Error,
    )


def get_market_orderbook(figi: str, depth: int) -> Request[OrderbookResponse, Error]:
    """
    Get order book by figi.

    Args:
        figi (str): FIGI
        depth (int): Depth of order book [1..20]

    Returns:
        Request[OrderbookResponse, Error]: Prepared request.
    """
    return prepare_request(
        method="GET",
        url="/market/orderbook",
        success=OrderbookResponse,
        failure=Error,
        params=dict(figi=figi, depth=depth),
    )


def get_market_candles(
    figi: str,
    from_datetime: datetime,
    to_datetime: datetime,
    interval: Literal[
        "1min",
        "2min",
        "3min",
        "5min",
        "10min",
        "15min",
        "30min",
        "hour",
        "day",
        "week",
        "month",
    ],
) -> Request[CandlesResponse, Error]:
    return prepare_request(
        method="GET",
        url="/market/candles",
        success=CandlesResponse,
        failure=Error,
        params={
            "figi": figi,
            "from": from_datetime.replace(tzinfo=timezone.utc).isoformat(),
            "to": to_datetime.replace(tzinfo=timezone.utc).isoformat(),
            "interval": interval,
        },
    )


def get_market_search_by_figi(
    figi: str,
) -> Request[SearchMarketInstrumentResponse, Error]:
    return prepare_request(
        method="GET",
        url="/market/search/by-figi",
        success=SearchMarketInstrumentResponse,
        failure=Error,
        params=dict(figi=figi),
    )


def get_market_search_by_ticker(
    ticker: str,
) -> Request[SearchMarketInstrumentResponse, Error]:
    return prepare_request(
        method="GET",
        url="/market/search/by-ticker",
        success=SearchMarketInstrumentResponse,
        failure=Error,
        params=dict(ticker=ticker),
    )
