from typing import Optional

from beanie import Document, Indexed, init_beanie
from motor.motor_asyncio import AsyncIOMotorClient
from ..schemas.market import Candle, MarketInstrument


class Instrument(Document):
    name: str
    figi: Indexed(str, unique=True)  # type: ignore
    isin: Optional[str]
    ticker: str
    currency: str
    type: str
    candles: list[Candle]


def response_to_document(
    instrument: MarketInstrument, candles: list[Candle]
) -> Instrument:
    return Instrument(
        name=instrument.name,
        figi=instrument.figi,
        isin=instrument.isin,
        ticker=instrument.ticker,
        currency=instrument.currency.value,
        type=instrument.type.value,
        candles=candles,
    )


async def init_db(db_uri: Optional[str]) -> AsyncIOMotorClient:
    client = AsyncIOMotorClient(db_uri)
    await init_beanie(database=client.Investments, document_models=[Instrument])
    return client
