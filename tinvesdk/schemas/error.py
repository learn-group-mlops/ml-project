from pydantic import Field

from .basemodel import BaseModel

__all__ = [
    "Error",
]


class ErrorPayload(BaseModel):
    message: str
    code: str


class Error(BaseModel):
    tracking_id: str = Field(alias="trackingId")
    status: str = "Error"
    payload: ErrorPayload
