from decimal import Decimal
from typing import Optional

from pydantic import Field

from .basemodel import BaseModel
from .enums import Currency, InstrumentType

__all__ = [
    "PortfolioResponse",
    "PortfolioCurrenciesResponse",
]


class MoneyAmount(BaseModel):
    currency: Currency
    value: Decimal


class PortfolioPosition(BaseModel):
    figi: str
    ticker: Optional[str]
    isin: Optional[str]
    instrument_type: InstrumentType = Field(alias="instrumentType")
    balance: Decimal
    blocked: Optional[Decimal]
    expected_yield: Optional[MoneyAmount] = Field(alias="expectedYield")
    lots: int
    average_position_price: Optional[MoneyAmount] = Field(alias="averagePositionPrice")
    average_position_price_no_nkd: Optional[MoneyAmount] = Field(
        alias="averagePositionPriceNoNkd"
    )
    name: str


class Portfolio(BaseModel):
    positions: list[PortfolioPosition]


class PortfolioResponse(BaseModel):
    tracking_id: str = Field(alias="trackingId")
    status: str = "Ok"
    payload: Portfolio


class CurrencyPosition(BaseModel):
    balance: Decimal
    blocked: Optional[Decimal]
    currency: Currency


class Currencies(BaseModel):
    currencies: list[CurrencyPosition]


class PortfolioCurrenciesResponse(BaseModel):
    payload: Currencies
    status: str = "Ok"
    tracking_id: str = Field(alias="trackingId")
