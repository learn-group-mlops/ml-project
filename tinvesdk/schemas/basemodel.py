import orjson
from pydantic import BaseModel as _BaseModel

__all__ = [
    "BaseModel",
]


class BaseModel(_BaseModel):
    class Config:
        @staticmethod
        def orjson_dumps(value, *, default):
            """
            orjson.dumps returns bytes, to match standard json.dumps we need
            to decode.
            """
            return orjson.dumps(value, default=default).decode()

        json_loads = orjson.loads
        json_dumps = orjson_dumps
        allow_mutation = False
