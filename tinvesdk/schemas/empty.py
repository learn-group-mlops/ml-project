from pydantic import Field

from .basemodel import BaseModel

__all__ = [
    "Empty",
]


class EmptyPayload(BaseModel):
    pass


class Empty(BaseModel):
    tracking_id: str = Field(alias="trackingId")
    payload: EmptyPayload
    status: str = "Ok"
