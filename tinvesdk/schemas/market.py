from datetime import datetime
from decimal import Decimal
from typing import Optional

from pydantic import Field

from .basemodel import BaseModel
from .enums import CandleResolution, Currency, InstrumentType, TradeStatus

__all__ = [
    "MarketInstrumentListResponse",
    "OrderbookResponse",
    "CandlesResponse",
    "SearchMarketInstrumentResponse",
]


class MarketInstrument(BaseModel):
    figi: str
    ticker: str
    isin: Optional[str]
    min_price_increment: Optional[Decimal] = Field(alias="minPriceIncrement")
    lot: int
    min_quantity: Optional[int] = Field(alias="minQuantity")
    currency: Currency
    name: str
    type: InstrumentType


class MarketInstrumentList(BaseModel):
    total: Decimal
    instruments: list[MarketInstrument]


class MarketInstrumentListResponse(BaseModel):
    tracking_id: str = Field(alias="trackingId")
    status: str = "Ok"
    payload: MarketInstrumentList


class OrderResponse(BaseModel):
    price: Decimal
    quantity: int


class Orderbook(BaseModel):
    figi: str
    depth: int
    bids: list[OrderResponse]
    asks: list[OrderResponse]
    trade_status: TradeStatus = Field(alias="tradeStatus")
    min_price_increment: Decimal = Field(alias="minPriceIncrement")
    face_value: Optional[Decimal] = Field(alias="faceValue")
    last_price: Optional[float] = Field(alias="lastPrice")
    close_price: Optional[float] = Field(alias="closePrice")
    limit_up: Optional[float] = Field(alias="limitUp")
    limit_down: Optional[float] = Field(alias="limitDown")


class OrderbookResponse(BaseModel):
    tracking_id: str = Field(alias="trackingId")
    status: str = "Ok"
    payload: Orderbook


class Candle(BaseModel):
    figi: str
    interval: CandleResolution
    o: Decimal
    c: Decimal
    h: Decimal
    l: Decimal
    v: int
    time: datetime


class Candles(BaseModel):
    figi: str
    interval: CandleResolution
    candles: list[Candle]


class CandlesResponse(BaseModel):
    tracking_id: str = Field(alias="trackingId")
    status: str = "Ok"
    payload: Candles


class SearchMarketInstrument(BaseModel):
    figi: str
    ticker: str
    isin: Optional[str]
    min_price_increment: Optional[Decimal] = Field(alias="minPriceIncrement")
    lot: int
    currency: Optional[Currency]
    name: str
    type: InstrumentType


class SearchMarketInstrumentResponse(BaseModel):
    tracking_id: str = Field(alias="trackingId")
    status: str = "Ok"
    payload: SearchMarketInstrument
