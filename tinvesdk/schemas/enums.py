from enum import Enum

__all__ = [
    "Currency",
    "InstrumentType",
]


class Currency(str, Enum):
    RUB = "RUB"
    USD = "USD"
    EUR = "EUR"
    GBP = "GBP"
    HKD = "HKD"
    CHF = "CHF"
    JPY = "JPY"
    CNY = "CNY"
    TRY = "TRY"


class InstrumentType(str, Enum):
    stock = "Stock"
    currency = "Currency"
    bond = "Bond"
    etf = "Etf"


class TradeStatus(str, Enum):
    normal_trading = "NormalTrading"
    not_available_for_trading = "NotAvailableForTrading"


class CandleResolution(str, Enum):
    """
    Интервал свечи и допустимый промежуток запроса:
    - min1 [1 minute, 1 day]
    - min2 [2 minutes, 1 day]
    - min3 [3 minutes, 1 day]
    - min5 [5 minutes, 1 day]
    - min10 [10 minutes, 1 day]
    - min15 [15 minutes, 1 day]
    - min30 [30 minutes, 1 day]
    - hour [1 hour, 7 days]
    - day [1 day, 1 year]
    - week [7 days, 2 years]
    - month [1 month, 10 years]
    """

    min1 = "1min"
    min2 = "2min"
    min3 = "3min"
    min5 = "5min"
    min10 = "10min"
    min15 = "15min"
    min30 = "30min"
    hour = "hour"
    day = "day"
    week = "week"
    month = "month"
