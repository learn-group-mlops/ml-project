import asyncio
from datetime import datetime, date, timedelta
from typing import TypeVar

import dotenv
import httpx
from dateutil.rrule import YEARLY, rrule
from returns.future import FutureResult
from returns.result import Failure, Result, Success
from tqdm import tqdm

from tinvesdk.preprequests import (
    get_market_bonds,
    get_market_candles,
    get_market_currencies,
    get_market_etfs,
    get_market_stocks,
)
from tinvesdk.schemas.basemodel import BaseModel
from tinvesdk.database.beanie_setup import Instrument, init_db, response_to_document
from beanie.operators import AddToSet

S = TypeVar("S", bound=BaseModel)
F = TypeVar("F", bound=BaseModel)


class TooManyRequestsError(Exception):
    pass


def unwrap_response_result(response: Result[S, F] | FutureResult[S, F]) -> S | None:
    match response:
        case Success(value):
            return value
        case Failure(value) if value is TooManyRequestsError:
            raise TooManyRequestsError
        case Failure(value):
            raise Exception(value)


def sudo_make_request(client, instrument, from_, to_):
    while True:
        try:
            response = unwrap_response_result(
                get_market_candles(
                    instrument.figi,
                    from_datetime=from_,
                    to_datetime=to_,
                    interval="day",
                )(client.request)
            )
            return response
        except TooManyRequestsError:
            continue


async def get_all_data(
    from_datetime: datetime,
    to_datetime: datetime,
    token=dotenv.get_key(".env", "API_TOKEN_SANDBOX"),
):
    client = httpx.Client(
        base_url="https://api-invest.tinkoff.ru/openapi/sandbox/",
        headers={"accept": "application/json", "Authorization": f"Bearer {token}"},
    )
    await init_db(dotenv.get_key(".env", "DB_URI"))
    stocks_list = unwrap_response_result(get_market_stocks()(client.request))
    bonds_list = unwrap_response_result(get_market_bonds()(client.request))
    etfs_list = unwrap_response_result(get_market_etfs()(client.request))
    currencies_list = unwrap_response_result(get_market_currencies()(client.request))
    if stocks_list and bonds_list and etfs_list and currencies_list:
        datetimes = list(rrule(freq=YEARLY, dtstart=from_datetime, until=to_datetime))
        datetime_intervals = (
            list(zip(datetimes, datetimes[1:]))
            if len(datetimes) > 1
            else [(from_datetime, to_datetime)]
        )
        instruments_list = (
            stocks_list.payload.instruments
            + bonds_list.payload.instruments
            + etfs_list.payload.instruments
            + currencies_list.payload.instruments
        )
        for instrument in tqdm(instruments_list):
            candles_data = []
            for start, until in tqdm(datetime_intervals, leave=False):
                response = sudo_make_request(client, instrument, start, until)
                if response:
                    candles_data.extend(response.payload.candles)
            await Instrument.find_one(Instrument.figi == instrument.figi).upsert(
                AddToSet({Instrument.candles: {"$each": candles_data}}),
                on_insert=response_to_document(instrument, candles_data),
            )


if __name__ == "__main__":
    today = datetime.combine(date.today(), datetime.min.time())
    asyncio.run(get_all_data(today - timedelta(days=7), today))
