from airflow import DAG
from airflow.operators.bash import BashOperator
from datetime import datetime, timedelta
from airflow.providers.docker.operators.docker import DockerOperator

with DAG(
    dag_id="docker_dag",
    tags=['example'],
    start_date= datetime(2021, 12, 12),
    end_date= datetime(2022, 1, 3),
    catchup=False,
    dagrun_timeout= timedelta(minutes=5),
    schedule_interval="0 0 * * 0",
) as dag:
    t1 = BashOperator(task_id="start", bash_command='echo "Start data"')
    t2 = DockerOperator(
        task_id="docker_command",
        image="qio0/update-data",
        api_version="auto",
        auto_remove=True,
        docker_url="unix://var/run/docker.sock",
        network_mode="bridge",
    )
    t3 = BashOperator( task_id="ready", bash_command='echo "Data ready"')
    t1 >> t2 >> t3
